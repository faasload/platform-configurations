# Knative for FaaSLoad

The currently available method to deploy Knative is on a [minikube cluster](https://minikube.sigs.k8s.io/docs/).
However it is still possible to use [kind](https://kind.sigs.k8s.io/), or a normal Kubernetes cluster (with [Rancher](https://www.rancher.com/) for instance).

## minikube

### Pre-requisites

They will be checked by the deployment script.

1. [minikube](https://minikube.sigs.k8s.io/docs/)
2. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

### Deployment

Run the script [`deploy.sh`](deploy.sh):

```sh
# Argument (see below): PROFILE_NAME.
./deploy.sh PROFILE_NAME
```

* `PROFILE_NAME` is the name of the profile that minikube will create for the deployment (default: `faasload`)

The script:

0. deletes a possibly existing minikube cluster with the given profile name
1. creates a minikube cluster under the given profile name
2. stops to have you run a command manually: simply follow the instructions, and when it is done, press any key to resume the script
3. finishes the configuration of the minikube cluster
4. deploys Knative on the cluster
5. applies configuration tweaks for use with FaaSLoad

The deployment is based on [this official documentation](https://knative.dev/docs/install/yaml-install/serving/install-serving-with-yaml/).

## Other targets

If another method such as kind or a normal Kubernetes cluster is used, the custom ConfigMap [`custom-configmap.yaml`](custom-configmap.yaml) may need adjustements.

The role of this ConfigMap is to allow Knative to log a function invocation request on the Pod that handles it, and to add tracing of the request handling path from the gateway to the Pod.
