# Apache OpenWhisk for FaaSLoad with Ansible

_This deployment method is unmaintained, as FaaSLoad now relies more on the FaaS platform being deployed on a Kubernetes cluster.
Deploying OpenWhisk probably still works, but running FaaSLoad on it may require extra work.
Prefer the [kind](../kind) method for a local deployment._

Apache OpenWhisk provides [Ansible](https://docs.ansible.com/) playbooks to deploy it locally, in [the official repository](https://github.com/apache/openwhisk/tree/master/ansible).
This is intended for easy local development of OpenWhisk's components.

## Pre-requisites

They will be checked by the deployment script.

### Native

Binaries that must be available in your `PATH`.

1. `git`
  * the deployment script will fetch OpenWhisk's source code (a fork of the official source code that includes fixes, bundled in this git repository as a submodule)
2. `python3`
3. `pip3`
4. `pipenv`
  * Ansible is a Python executable, and you are advised to use the provided [`Pipfile`](Pipfile) to install it in a virtual environment, thus the dependency on `pipenv`.
5. `java`
6. `npm`
7. `zip`
8. `docker`
9. `netcat`
  * In particular, it must be OpenBSD's `netcat`, that supports the `-U` option for UNIX sockets
    * This is the package `netcat-openbsd` for Debian and co., and the package `openbsd-netcat` on ArchLinux

### Python

A [`Pipfile`](Pipfile) is provided (and its version-locked counterpart `Pipfile.lock`), to be used with [pipenv](https://github.com/pypa/pipenv):

```sh
pipenv install
```

This command will install the following dependencies in a virtual environment, with the right version of Ansible for OpenWhisk's playbooks.

1. `ansible`
2. `docker`

### Other configuration

* your current user must be able to run Docker commands (i.e., be a member of the group `docker`)
* Docker must be running

## Deployment

Run the script [`deploy.sh`](deploy.sh) **in the Python virtual environment** created by pipenv:

```sh
# Use pipenv run to run the command in the Python virtual environment.
pipenv run ./deploy.sh
```

You can also get a shell in the virtual environment with `pipenv shell`.

If the script fails to terminate successfully, you will have to either start from scratch (i.e., manually clean all its successful operations), or to run the commands manually.

### Custom configuration

FaaSLoad's custom configuration for OpenWhisk comes from [`faasload.yml`](faasload.yml).
It includes:

* path customization
* use Docker's normal interface (its socket) instead of `runc`
* raise resource limits
* enable user events (mandatory to get notifications about activations)

