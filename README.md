# Platform configurations

Platform configurations and modifications to make them compatible with [FaaSLoad](https://gitlab.com/faasload/faasload).

There are currently two compatible platforms: 

* [Apache Openwhisk](openwhisk)
* [Knative](knative)

Refer to the respective directories for more instructions to deploy them.

## Why is it needed?

FaaSLoad is essentially platform-agnostic (although it assumes that the target FaaS platform eventually runs on Kubernetes).

This repository holds:

* deployment scripts for the FaaS platforms
* configurations for the FaaS platforms better suited for usage with FaaSLoad
* minor modifications (non source code) to allow monitoring of the FaaS platforms by FaaSLoad

