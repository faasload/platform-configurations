#! /bin/bash

# Deploy OpenWhisk with custom settings for use with FaaSLoad
#
# The script does the following:
# 
# * check the dependencies (native, and Python);
# * check Docker is running and available to the current user;
# * "install" Apache OpenWhisk by cloning its code in a subdirectory;
# * run its containers by deploying it with Ansible, as explained here:
#   https://github.com/apache/openwhisk/tree/master/ansible.
#
# Before running the script, make sure you installed the required native and
# Python dependencies. For the latter, use `pipenv install` to install them in a
# virtual environment, and then use `pipenv shell` to activate the virtualenv.

#=== Settings ==================================================================
OPENWHISK_HOME="$(dirname $0)/../openwhisk"
OPENWHISK_TMP_DIR="$OPENWHISK_HOME/ansible/tmp"

DEPENDENCIES="git python3 pip3 pipenv java npm zip docker"
PIP_DEPENDENCIES="ansible docker"

DOCKER_GROUP="docker"
DOCKER_PROBE_CMD="docker ps"

FAASLOAD_CUSTOM_CFG="$PWD/faasload.yml"
#===============================================================================

die() {
    echo "$*" 1>&2
    exit 1
}


for dep in $DEPENDENCIES; do
    if ! which "$dep" >/dev/null 2>&1; then
      echo "$dep is missing in your \$PATH" >&2
      failed_dependencies_check=y
    fi
done
if [[ -z $(nc -h 2>&1 | grep '\-U') ]]
then
    echo "Please ensure to have the OpenBSD version of netcat to support '-U' option"
    failed_dependencies_check=y
fi
if [ "$failed_dependencies_check" = "y" ]; then
    echo "failed dependency check, please install missing dependencies or fix your \$PATH" >&2
    exit 2
fi
for dep in $PIP_DEPENDENCIES; do
    if ! pip3 list --format=freeze 2>/dev/null | grep -q "$dep"; then
        echo "Python 3 pip dependency $dep is missing" >&2
        failed_dependencies_check=y
    fi
done
if [ "$failed_dependencies_check" = "y" ]; then
    echo "failed Python 3 pip dependency check, please install missing dependencies with \`pipenv install\` and activate the virtualenv with \`pipenv shell\`" >&2
    exit 2
fi

if ! id | grep -q "$DOCKER_GROUP"; then
    echo "your current user is not in the Docker group \"$DOCKER_GROUP\", you will not be able to deploy OpenWhisk" >&2
    echo "fix with: "'`usermod -aG "'"$DOCKER_GROUP"'" "'"$USER"'"`'", and then relog" >&2
    exit 2
fi
if ! $DOCKER_PROBE_CMD >/dev/null 2>&1; then
    echo "failed running Docker probe command \`$DOCKER_PROBE_CMD\`: is Docker running?" >&2
    exit 2
fi

if git submodule status "$OPENWHISK_HOME" | grep --quiet '^-'; then
    echo "updating OpenWhisk submodule at $OPENWHISK_HOME"
    git submodule update --init --recursive "$OPENWHISK_HOME" ||
        die "failed updating OpenWhisk submodule"
fi

mkdir -p "$OPENWHISK_TMP_DIR" ||
    die "failed creating tmp directory for OpenWhisk at \"$OPENWHISK_TMP_DIR\""

(
    cd "$OPENWHISK_HOME"

    (
        cd ansible

        ansible-playbook -i environments/local\
            -e ansible_python_interpreter="$(which python)"\
            -e @"$FAASLOAD_CUSTOM_CFG"\
            setup.yml
    ) ||
        die "failed running OpenWhisk's Ansible setup"

    ./gradlew distDocker ||
        die "failed building OpenWhisk"

    (
        cd ansible

        ansible-playbook -i environments/local \
            -e ansible_python_interpreter="$(which python)"\
            -e @"$FAASLOAD_CUSTOM_CFG" \
            couchdb.yml initdb.yml wipe.yml openwhisk.yml
    ) ||
        die "failed deploying OpenWhisk"

    bin/wsk property set --apihost localhost &&
        bin/wsk property set --auth "$(cat ansible/files/auth.guest)" ||
        die "failed setting OpenWhisk API properties"
) ||
    die "failed installing and deploying OpenWhisk"

echo "Done installing and deploying OpenWhisk."
echo "Add \"$OPENWHISK_HOME/bin\" to your \$PATH, to use OpenWhisk CLI \`wsk\`."
echo "It is also suggested to set up an alias such as \`alias wsk=\"wsk -i\"\` "
echo "to avoid checking the certificate of the OpenWhisk REST API server (which "
echo "is broken in a local Ansible deployment)."

