# Apache OpenWhisk for FaaSLoad with kind

Apache OpenWhisk provides a script and a Helm chart in [the official repository](https://github.com/apache/openwhisk-deploy-kube/tree/master/deploy/kind) to deploy it locally, in a [kind](https://kind.sigs.k8s.io/) cluster.
This is intended for local testing.

## Limitations

**FaasLoad's monitor cannot be used in kind clusters.**

FaaSLoad's monitor requires access to Docker's socket to work, but it is not used by kind clusters, because it runs "bare" containers inside Docker containers.

## Pre-requisites

Binaries that must be available in your `PATH`.

1. `git`
  * the deployment script will fetch OpenWhisk's repository of deployment script and Helm charts (a fork of the official content that includes improvements, bundled in this git repository as a submodule)
2. `kind`
3. `helm`
4. `kubectl`

## Deployment

Run the script [`deploy.sh`](deploy.sh):

```sh
./deploy.sh
```

It will deploy OpenWhisk with the configuration provided in [`mycluster.yaml`](mycluster.yaml).

If re-run, it will check for already successful deployment steps, and reuse their results.

### Advanced usage

This script is just a wrapper of the actual deployment script, so if you need more customization, for example:

* a configuration file for the kind cluster that is different from the default one provided by OpenWhisk
* run only up to a certain deployment step

Please directly use the actual deployment script from OpenWhisk's repository of deployment script and Helm chart;
it is located at the path given by the deployment script when run with `--help` (probably [`../openwhisk-deploy-kube/deploy/kind/deploy.sh`](../openwhisk-deploy-kube/deploy/kind/deploy.sh):

```sh
./deploy.sh --help
```

Run the actual deployment script with `--help` for more details.
