#! /usr/bin/bash

# Deploy OpenWhisk in a Kubernetes cluster for FaaSLoad.
# This scripts assumes an already running cluster, with kubectl properly
# configured for it.
# To build a Kubernetes cluster fit for OpenWhisk, see:
# https://github.com/apache/openwhisk-deploy-kube/.
# In particular, you should set it up to labelling the invoker nodes, as
# explained here: https://github.com/apache/openwhisk-deploy-kube/#multi-worker-node-clusters.


# CONFIGURATION {{{
SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
OPENWHISK_DEPLOY_KUBE_DIR="$(realpath "$SCRIPTDIR/../openwhisk-deploy-kube")"
BINDIR="$OPENWHISK_DEPLOY_KUBE_DIR/bin"

CLI="$BINDIR/wsk"

# Template file for Helm chart values.
# See help of argument --helm-values.
HELM_VALUES_TMPL="mycluster.tmpl.yaml"
# Helm chart values file generated from processing the template file.
HELM_VALUES_GEN="mycluster.gen.yaml"
# Timeout to wait for Helm chart installation.
HELM_INSTALL_TIMEOUT="10m0s"

# Port of the Kubernetes NodePort to expose OpenWhisk's NGINX API gateway.
# This is the port defined in mycluster.tmpl.yaml.
# See help of argument --api-host.
NGINX_NODEPORT_PORT=31001

# Helm release of the deployment of OpenWhisk.
REL="ow"
# Namespace of the deployment of OpenWhisk in the Kubernetes cluster.
NS="openwhisk"

CLI_DOWNLOAD_URL="https://github.com/apache/openwhisk-cli/releases/download/1.2.0/OpenWhisk_CLI-1.2.0-linux-amd64.tgz"

AUTH="23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP"
# CONFIGURATION }}}

# DISPLAY ROUTINES {{{
if tput colors >/dev/null 2>&1; then
    reset="$(tput sgr0)"

    bold="$(tput bold)"
    # tput does not have a default capability to turn bold off, but most often
    # it is 22.
    normal="\e[22m"
    underline="$(tput smul)"
    nounderline="$(tput rmul)"

    red="$(tput setaf 1)"
    green="$(tput setaf 2)"
    yellow="$(tput setaf 3)"
    blue="$(tput setaf 4)"
fi

echo_info() {
    echo -e "$blue$*$reset"
}

echo_warn() {
    echo -e "$yellow$*$reset"
}

echo_err() {
    echo -e "$red$*$reset" 1>&2
}

die() {
    echo_err "$*"
    exit 1
}
# DISPLAY ROUTINES }}}

# OTHER ROUTINES {{{
quit_or_continue() {
    if [ "$stop_after" = "$1" ]; then
        shift
        echo_info "stopping after $*"
        exit 0;
    fi
}
# OTHER ROUTINES }}}

# DEPENDENCY CHECKS {{{
if ! which helm > /dev/null; then
    echo_err "missing dependency: helm"
    echo_err "see: https://helm.sh/docs/intro/install/"
    exit 3
fi

if ! which kubectl >/dev/null; then
    echo_err "missing dependency: kubectl"
    echo_err "see: https://kubernetes.io/docs/tasks/tools/#kubectl"
    exit 3
fi
# DEPENDENCY CHECKS }}}

# ARGUMENTS {{{
# See usage message.
api_host=""
helm_values=""
stop_after=""

echo_usage() {
    echo -e "Usage: $bold$red$0$reset [--api-host API_HOST] [--helm-values FILE] [--stop-after STEP]"
    echo
    echo -e "Deploy OpenWhisk to a Kubernetes cluster."
    echo
    echo -e "Options (only long forms):"
    echo
    echo -e "\t$bold$red--api-host ${green}API_HOST$reset"
    echo -e "\t\tOpenWhisk API host (including port)."
    echo -e "\t\tThis can be the host name or the IP address to reach the API host of OpenWhisk, with its public port."
    echo -e "\t\tBy default, this script uses the cluster-internal IP address of the first node in the list, and port $bold$NGINX_NODEPORT_PORT$normal as defined in \"$HELM_VALUES_TMPL\"."
    echo -e "\t\tOverride it if the node is not directly addressable, or if you use another port in your custom Helm chart values file (see $bold${red}--helm-values$reset)."
    echo
    echo -e "\t$bold$red--helm-values ${green}FILE$reset"
    echo -e "\t\tUse $bold${green}FILE$reset as a file of chart values to deploy the Helm chart of OpenWhisk."
    echo -e "\t\tIf not specified, the template file \"$HELM_VALUES_TMPL\" is used."
    echo -e "\t\tThe template file is processed into \"$HELM_VALUES_GEN\" by replacing with their values, the following placeholders written between double-braces;"
    echo -e "\t\tfor example '{{ ingress.apiHostName }}'."
    echo -e "\t\t\t$bold${green}ingress.apiHostName$reset: API host name (or IP address), without port"
    echo -e "\t\t\t$bold${green}ingress.apihostPort$reset: API host port"
    echo
    echo -e "\t$bold$red--stop-after ${green}STEP$reset"
    echo -e "\t\tStop the deployment after $bold${green}STEP$reset."
    echo -e "\t\tRunning $bold$0$reset again will pick up where it left."
    echo
    echo -e "\t\tSteps:"
    echo -e "\t\t\t$bold${green}init$reset: check dependencies and parse arguments"
    echo -e "\t\t\t$bold${green}cluster-test$reset: test the Kubernetes cluster and generate \"$HELM_VALUES_GEN\" if required"
    echo -e "\t\t\t$bold${green}helm-chart$reset: install the Helm chart"
    echo -e "\t\t\t$bold${green}cli$reset: download the OpenWhisk CLI"
    echo
    echo -e "\t$bold$red--help$reset"
    echo -e "\t\tDisplay this help text."
}

check_param_value() {
    if [ -z "$2" ]; then
        echo_err "missing value for option \"$1\""
        echo_usage
        exit 2
    fi
}

while [ $# -gt 0 ]; do
    case "${1#--}" in
        "api-host")
            check_param_value "$1" "$2"
            api_host="$2"
            shift 2

            echo_info "using OpenWhisk API host $bold$api_host$normal"
            ;;
        "helm-values")
            check_param_value "$1" "$2"
            helm_values="$2"
            shift 2

            echo_info "using Helm chart values file $bold$helm_values$normal"
            ;;
        "stop-after")
            check_param_value "$1" "$2"
            stop_after="$2"
            shift 2

            case "$stop_after" in
                "init"|"cluster-test"|"helm-chart"|"cli")
                    echo_info "the script will stop after step $bold$stop_after$normal";;
                *)
                    echo_err "unknown step to stop at: $bold$stop_after$reset"
                    echo
                    echo_usage
                    exit 2
            esac
            ;;
        "help")
            echo_usage
            exit 0
            ;;
        *)
            echo_err "unknown option \"$1\""
            echo
            echo_usage
            exit 2
    esac
    # Bash does not care if there are not enough remaining arguments to shift.
done
# ARGUMENTS }}}

if git submodule status "$OPENWHISK_DEPLOY_KUBE_DIR" | grep --quiet '^-'; then
    echo_info "updating OpenWhisk Kubernetes deployment submodule at $bold$OPENWHISK_DEPLOY_KUBE_DIR$normal"
    git submodule update --init --recursive "$OPENWHISK_DEPLOY_KUBE_DIR" ||
        die "failed updating OpenWhisk Kubernetes deployment submodule"
else
    echo_warn "using existing OpenWhisk Kubernetes deployment submodule at $bold$OPENWHISK_DEPLOY_KUBE_DIR$normal"
fi

quit_or_continue "init" "initializing"

if [ -z "$api_host" ]; then
    api_host="$(kubectl get nodes --output jsonpath='{.items[0].status.addresses[?(@.type=="InternalIP")].address}' 2>/dev/null)":$NGINX_NODEPORT_PORT
    [ $? -ne 0 ] && die "failed getting OpenWhisk API host IP address"
fi

echo_info "using $bold$api_host$normal as OpenWhisk API host"

if ! kubectl get nodes --output jsonpath='{.items[*].metadata.labels.openwhisk-role}' | grep -q 'invoker'; then
    die "no Kubernetes nodes labeled 'openwhisk-role=invoker', you must label at least one to run the health test action and end the deployment"
fi

if [ -z "$helm_values" ]; then
    helm_values="$HELM_VALUES_GEN"

    if [ -f "$helm_values" ]; then
        echo_warn "using existing generated Helm values file $bold$helm_values$normal"
    else
        sed 's/{{ ingress.apiHostName }}/"'"${api_host%:*}"'"/
             s/{{ ingress.apiHostPort }}/'"${api_host#*:}"'/' "$HELM_VALUES_TMPL" > "$helm_values"

        echo_info "templated $bold$HELM_VALUES_TMPL$normal into $bold$helm_values$normal"
    fi
fi

quit_or_continue "cluster-test" "testing the Kubernetes cluster"

# Install the Helm chart of OpenWhisk.
if ! helm --namespace "$NS" status "$REL" >/dev/null 2>&1; then
    echo_info "installing Helm chart of OpenWhisk in namespace $bold$NS$normal under release $bold$REL$normal"
    echo_warn "this will wait for the end of the installation (up to $HELM_INSTALL_TIMEOUT)"

    helm install "$REL" "$OPENWHISK_DEPLOY_KUBE_DIR/helm/openwhisk"\
        --namespace "$NS" --create-namespace\
        --values "$helm_values"\
        --wait --timeout "$HELM_INSTALL_TIMEOUT" ||
        die "failed deploying OpenWhisk with Helm"

    echo_warn "this deployment does not install packages; waiting for the deployment of the invokers instead"
else
    echo_warn "using existing Helm release $bold$REL$normal in namespace $bold$NS$normal for OpenWhisk"
fi

quit_or_continue "helm-chart" "installing Helm chart"

# Set up OpenWhisk CLI.
if [ ! -x "$CLI" ]; then
    echo_info "downloading OpenWhisk CLI to $bold\"$CLI\"$normal"

    mkdir -p "$BINDIR"
    curl -L "$CLI_DOWNLOAD_URL" | tar -C "$BINDIR" -xz wsk ||
        die "failed downloading wsk CLI"
else
    echo_warn "using existing OpenWhisk CLI program at $bold\"$CLI\"$normal"
fi

quit_or_continue "cli" "downloading OpenWhisk CLI"

if [ "$("$CLI" property get --output raw --apihost)" != "$api_host" ]; then
    echo_warn "USER ACTION REQUIRED: configure API host in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --apihost \"$api_host\"\`$normal"
else
    echo_warn "API host in OpenWhisk CLI already configured to use the Kubernetes cluster at $bold\"$api_host\"$normal"
fi

if [ "$("$CLI" property get --output raw --auth)" != "$AUTH" ]; then
    echo_warn "USER ACTION REQUIRED: configure auth in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --auth \"$AUTH\"\`$normal"
else
    echo_warn "auth in OpenWhisk CLI already configured for the Kubernetes cluster"
fi

echo_info "done"


