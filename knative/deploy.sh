#! /bin/bash

die() {
    echo "$*" 1>&2
    exit 1
}

NETWORK_BRIDGE=faasload

if ! [[ $DAEMONS =~ (^|[[:space:]])"$1"($|[[:space:]]) ]] ; then
  echo "Unknown runtime" >&2
  exit 2
fi

if ! which "minikube" > /dev/null 2>&1 || ! which "kubectl" > /dev/null 2>&1; then
  echo "minikube and kubectl are required in your \$PATH." >&2
  exit 2
fi

if [ ! $# -lt 1 ]; then
  NETWORK_BRIDGE=$1
fi

(
  minikube -p $NETWORK_BRIDGE delete
  minikube -p $NETWORK_BRIDGE start
  echo ""
  echo "To finish setting up networking for minikube, run the following command in a separate terminal window:"
  echo "minikube -p $NETWORK_BRIDGE tunnel"
  echo ""
  echo 'Press any key to continue'
  while [ true ] ; do read -t 3 -n 1; if [ $? = 0 ] ; then break; fi done
  (
    minikube -p $NETWORK_BRIDGE addons enable registry
  ) || die 'failed enable minikube registry'
  (
    kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.6.0/serving-crds.yaml
    kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.6.0/serving-core.yaml
  ) || die "failed deploying knative serving"
  (
    kubectl apply -f https://github.com/knative/net-kourier/releases/download/knative-v1.6.0/kourier.yaml
    kubectl patch configmap/config-network \
      --namespace knative-serving \
      --type merge \
      --patch '{"data":{"ingress-class":"kourier.ingress.networking.knative.dev"}}'
  ) || die "failed deploying networks inside the cluster"
  (
    kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.6.0/serving-default-domain.yaml
  ) || die "failed configuring dns"
  (
    kubectl apply -f jaeger.yaml
    kubectl apply -f custom-configmap.yaml
  ) || die "failed updating the configuration for tracing purpose"
) || die "failed deploying the platform"
