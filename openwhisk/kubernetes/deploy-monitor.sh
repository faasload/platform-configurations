#! /usr/bin/bash

# Deploy FaaSLoad's monitor on every worker node of the cluster.
# This scripts assumes an already running cluster, with kubectl properly
# configured for it.
# To build a Kubernetes cluster fit for OpenWhisk, see:
# https://github.com/apache/openwhisk-deploy-kube/.
# In particular, you should set it up to labelling the invoker nodes, as
# explained here: https://github.com/apache/openwhisk-deploy-kube/#initial-setup.


# CONFIGURATION {{{
# Namespace of the deployment of FaaSLoad's components in the Kubernetes cluster.
NS="faasload"

# File of Kubernetes configuration to apply to deploy the monitor's daemon set.
MONITOR_DAEMONSET_FILE="monitor.yaml"
# Name of the config map for the monitor.
CONFIGMAP_NAME="monitor"
# Name of the daemon set for the monitor pods.
# Must the same as in the file given by MONITOR_DAEMONSET_FILE.
DAEMONSET_NAME="faasload-monitor"
# CONFIGURATION }}}

# DISPLAY ROUTINES {{{
if tput colors >/dev/null 2>&1; then
    reset="$(tput sgr0)"

    bold="$(tput bold)"
    # tput does not have a default capability to turn bold off, but most often
    # it is 21.
    normal="\e[21m"
    underline="$(tput smul)"
    nounderline="$(tput rmul)"

    red="$(tput setaf 1)"
    green="$(tput setaf 2)"
    yellow="$(tput setaf 3)"
    blue="$(tput setaf 4)"
fi

echo_info() {
    echo -e "$blue$*$reset"
}

echo_warn() {
    echo -e "$yellow$*$reset"
}

echo_err() {
    echo -e "$red$*$reset" 1>&2
}

die() {
    echo_err "$*"
    exit 1
}
# DISPLAY ROUTINES }}}

# DEPENDENCY CHECKS {{{
if ! which kubectl >/dev/null; then
    echo_err "missing dependency: kubectl"
    echo_err "see: https://kubernetes.io/docs/tasks/tools/#kubectl"
    exit 3
fi
# DEPENDENCY CHECKS }}}

# ARGUMENTS {{{
# See usage message.
config_file=""

echo_usage() {
    echo -e "Usage: $bold$red$0$reset MONITOR_CONFIG_FILE"
    echo
    echo -e "Deploy FaaSLoad's monitor."
    echo
    echo -e "Arguments:"
    echo
    echo -e "\t$bold${green}MONITOR_CONFIG_FILE$reset"
    echo -e "\t\tConfiguration file of the monitor."
    echo -e "\t\tIt is loaded to the cluster as the config map named $bold$CONFIGMAP_NAME$normal."
    echo
    echo -e "Options:"
    echo
    echo -e "\t$bold$red--help$reset"
    echo -e "\t\tDisplay this help text."
    echo
    echo -e "Deploy one pod per Kubernetes node where OpenWhisk may invoke actions."
    echo -e "The nodes are identified by the label ${bold}openwhisk-role=invoker$normal."
    echo -e "The monitor pod runs the Docker container image ${bold}faasload/monitor:local$normal;"
    echo -e "this image must be available to the Kubernetes workers, either from the Docker image registry, or by being built locally on each of them."
}

check_param_value() {
    if [ -z "$2" ]; then
        echo_err "missing value for option \"$1\""
        echo_usage
        exit 2
    fi
}

if [ $# -lt 1 ]; then
    echo_err "Missing argument: MONITOR_CONFIG_FILE"
    echo_usage
    exit 2
fi

while [ $# -gt 0 ]; do
    case "$1" in
        --*)          # flags
            case "${1#--}" in
                "help")
                    echo_usage
                    exit 0
                    ;;
                *)
                    echo_err "unknown option \"$1\""
                    echo
                    echo_usage
                    exit 2
            esac
            ;;
        *)            # arguments
            if [ -n "$config_file" ]; then
                echo_err "unexpected argument \"$1\""
                echo
                echo_usage
                exit 2
            else
                config_file="$1"
                shift 1

                echo_info "using monitor configuration file $bold$config_file$normal"
            fi
            ;;
    esac
    # Bash does not care if there are not enough remaining arguments to shift.
done

if [ -z "$config_file" ]; then
    echo_err "missing argument MONITOR_CONFIG_FILE"
    echo
    echo_usage

    exit 2
fi
# ARGUMENTS }}}

if ! kubectl get namespace "$NS" >/dev/null 2>&1; then
    echo_info "creating Kubernetes namespace $bold$NS$normal"
    kubectl create namespace "$NS" ||
        die "failed creating Kubernetes namespace"
else
    echo_warn "using existing Kubernetes namespace $bold$NS$normal"
fi

if ! kubectl --namespace "$NS" get configmap "$CONFIGMAP_NAME" >/dev/null 2>&1; then
    echo_info "creating config map $bold$CONFIGMAP_NAME$normal for the monitor's configuration"
    kubectl --namespace "$NS" create configmap "$CONFIGMAP_NAME" --from-file="$config_file" ||
        die "failed creating config map for the monitor's configuration"
else
    echo_warn "using existing config map $bold$CONFIGMAP_NAME$normal for the monitor's configuration"
fi

echo_info "deploying FaaSLoad's monitor as the daemon set $bold$DAEMONSET_NAME$normal"
kubectl --namespace "$NS" apply --filename "$MONITOR_DAEMONSET_FILE" ||
    die "failed applying Kubernetes configuration to create the monitor's daemon set"

