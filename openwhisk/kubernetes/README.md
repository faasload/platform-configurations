# Apache OpenWhisk for FaaSLoad on Kubernetes

Apache OpenWhisk provides a Helm chart in [the official repository](https://github.com/apache/openwhisk-deploy-kube/tree/master/helm) to deploy it on a Kubernetes cluster.
Note that this repository also offers scripts to deploy OpenWhisk on specific Kubernetes clusters, like Google Cloud Platform (GCP) or IMB Public Cloud.

## Pre-requisites

Binaries that must be available in your `PATH`.

1. `git`
  * the deployment script will fetch OpenWhisk's repository of deployment script and Helm charts (a fork of the official content that includes improvements, bundled in this git repository as a submodule)
3. `helm`
4. `kubectl`

## Deployment

### Setup of Kubernetes nodes

As instructed in ["Deploying OpenWhisk - Initial setup"](https://github.com/apache/openwhisk-deploy-kube/#multi-worker-node-clusters), you must label all Kubernetes nodes where you wish OpenWhisk to run user actions, with the label `openwhisk-role=invoker`.
This is too cluster-specific to be done by the deployment script;
however the deployment script will yield an error if there are no nodes labeled as such.

To label a node so user actions will run on it:

```sh
kubectl label node NODE_NAME "openwhisk-role=invoker"
```

In addition, on a low-node count cluster, you may want to remove the taint `node-role.kubernetes.io/control-plane` from Kubernetes's control plane node so OpenWhisk's pods (not just user actions) can be scheduled on it:

```sh
# Remove the taint from all nodes (so, including the control plane node).
# Note the dash "-" after the taint name, to remove it.
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
```

### Deployment of OpenWhisk

Run the script [`deploy.sh`](deploy.sh):

```sh
./deploy.sh
```

It will deploy OpenWhisk with the configuration provided in [`mycluster.tmpl.yaml`](mycluster.tmpl.yaml).
This is a template file that is processed by `deploy.sh` into `mycluster.gen.yaml`. 
It inserts the API host name and port of the deployed OpenWhisk platform (i.e., the ingress point for OpenWhisk in the cluster).
To customize the API host, pass the option `--api-host` to the deployment script.
For other customization, modify [`mycluster.tmpl.yaml`](mycluster.tmpl.yaml).

If re-run, the script will check for already successful deployment steps, and reuse their results.
In particular, **it will reuse `mycluster.gen.yaml`** instead of processing the template again.

See `./deploy.sh --help` for full information about the deployment script.

## Deployment of FaaSLoad's monitor

Run the script [`deploy-monitor.sh`](deploy-monitor.sh):

```sh
# Argument: MONITOR_FILE.
./deploy-monitor.sh MONITOR_FILE
```

* `MONITOR_FILE`: the configuration file of the monitor, e.g., see `config/monitor.yml` from FaaSLoad's repository

It will deploy one instance of the monitor on every node of the Kubernetes cluster.

See `./deploy-monitor.sh --help` for full information.
In particular, check the name of the container image that is used to run the container.
