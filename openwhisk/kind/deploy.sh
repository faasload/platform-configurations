#! /usr/bin/bash

# Deploy OpenWhisk in a kind cluster for FaaSLoad.

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
OPENWHISK_DEPLOY_KUBE_DIR="$(realpath "$SCRIPTDIR/../openwhisk-deploy-kube")"
DEPLOY_SCRIPT="$OPENWHISK_DEPLOY_KUBE_DIR/deploy/kind/deploy.sh"

HELM_MYCLUSTER_ADDON_FILE="$SCRIPTDIR/mycluster.yaml"

if [ "$1" = "--help" ]; then
    echo -e "Usage: $0"
    echo
    echo -e "Deploy OpenWhisk to a kind cluster."
    echo
    echo -e "This script calls the deployment script \"$DEPLOY_SCRIPT\" fetched in a Git submodule."
    echo -e "It uses the custom file of Helm addon values \"$HELM_MYCLUSTER_ADDON_FILE\"."
    echo -e "If you need further customization, please refer to the deployment script."
    exit 0
fi

if git submodule status "$OPENWHISK_DEPLOY_KUBE_DIR" | grep --quiet '^-'; then
    echo "updating OpenWhisk Kubernetes deployment submodule at $OPENWHISK_DEPLOY_KUBE_DIR"
    if ! git submodule update --init --recursive "$OPENWHISK_DEPLOY_KUBE_DIR"; then
        echo "failed updating OpenWhisk Kubernetes deployment submodule" >&2
        exit 1;
    fi
fi

# See $DEPLOY_SCRIPT --help for help.
"$DEPLOY_SCRIPT" --helm-addon-values "$HELM_MYCLUSTER_ADDON_FILE"

