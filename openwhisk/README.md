# Apache OpenWhisk for FaaSLoad

There are three deployment methods:

1. [On a normal Kubernetes cluster](kubernetes)
2. [On a local kind Kubernetes cluster](kind)
3. [Using Ansible](ansible) (unmaintained)

Refer to the respective directories for more instructions on each deployment method.

