# Change default tmp directory (for OpenWhisk's local configuration and logs) to "~/openwhisk/ansible/tmp" (same default as FaaSLoad's)
openwhisk_tmp_dir: "{{ lookup('env', 'OPENWHISK_TMP_DIR') | default(openwhisk_home ~ '/ansible/tmp', true) }}"
config_root_dir: "{{ openwhisk_tmp_dir }}/wskconf"
whisk_logs_dir: "{{ openwhisk_tmp_dir }}/wsklogs"
coverage_logs_dir: "{{ openwhisk_tmp_dir }}/wskcov"

# Disable using runc
# runc is the "universal container runtime" underneath Docker. OpenWhisk uses runc to pause and unpause containers for
# historical performance reasons (https://github.com/apache/openwhisk/issues/3711#issuecomment-774304573). They can be
# ignored for recent Docker versions. Yet OpenWhisk still defaults to using runc (except for Docker-for-Mac
# environments, which do not use runc).
# runc being "lower level", it does not use Docker's socket, so FaaSLoad cannot catch is Docker operations, i.e.,
# FaaSLoad cannot see pause and unpause operations (it seems that it also couldn't see kill/die/destroy operations).
# Thus, disable using runc unconditionally.
invoker_use_runc: false

# Increase memory available to the invoker to run user container
# This sets the total memory that the invoker will use to run any user action to answer invocation requests it receives.
# Maximum memory limit for user actions is set further below, by "limit_action_memory_max".
invoker_user_memory: 8192m

# Increase system per-namespace limits
limits:
  invocationsPerMinute: "{{ limit_invocations_per_minute | default(300) }}"
  concurrentInvocations: "{{ limit_invocations_concurrent | default(150) }}"
  firesPerMinute: "{{ limit_fires_per_minute | default(300) }}"
  sequenceMaxLength: "{{ limit_sequence_max_length | default(50) }}"

# Increase maximum action memory to 2GiB
limit_action_memory_max: 2048m

# Enable user_events metrics, used by FaaSLoad's loader to monitor activations
user_events: true
